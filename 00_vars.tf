variable "proxmox_url" {
  type = string
  default = ""
}
variable "token_id" {
 type = string
 default = ""
}
variable "token_secret" {
 type = string
 default = ""
}

variable "proxmox_host" {
  type = string 
  default = "pve-1"
}

variable "template_name" {
  type = string
  default = "Rocky-TerraTemplate"
}
variable "vm_name" {
  type = list(string)
  default = ["Saleor-Dev","Saleor-Production"]
}
variable "pool" {
  type = string
  default = "deusops_saleor"
}

variable "ssh_user" {
  type = string
  default = "root"
}

variable "ssh_key" {
  type = string
  default = ""
}

variable "network_bridge" {
  type = string
  default = "vmbr99"
}

